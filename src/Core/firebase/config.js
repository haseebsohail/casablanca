import { decode, encode } from 'base-64';
import './timerConfig';
global.addEventListener = (x) => x;
if (!global.btoa) {
  global.btoa = encode;
}

if (!global.atob) {
  global.atob = decode;
}

import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

// const firebaseConfig = {
//   apiKey: 'AIzaSyAOWHBpPhKoNhcGFKHH_Q_0AtL2gV-imgQ',
//   authDomain: 'production-a9404.firebaseapp.com',
//   databaseURL: 'https://production-a9404.firebaseio.com',
//   projectId: 'production-a9404',
//   storageBucket: 'production-a9404.appspot.com',
//   messagingSenderId: '525472070731',
//   appId: '1:525472070731:web:ee873bd62c0deb7eba61ce',
// };

const firebaseConfig = {
  apiKey: 'AIzaSyDQX-CrXi2v0C2qJLlJuXonkONkCOClQCM',
  authDomain: 'casablanca-dream.firebaseapp.com',
  databaseURL: 'https://casablanca-dream.firebaseio.com',
  projectId: 'casablanca-dream',
  storageBucket: 'casablanca-dream.appspot.com',
  messagingSenderId: '213246529677',
  appId: '1:213246529677:android:3e4d2d123add969c6a52c4',
  measurementId: 'G-5864WNE50N',
};

if (!firebase.apps.length) firebase.initializeApp(firebaseConfig);

export { firebase };
