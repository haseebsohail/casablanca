import React, { Component } from 'react';
import { TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';

export default class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Welcome');
    }, 4500);
  }
  render() {
    return (
      <Image
        style={{
          resizeMode: 'cover',
          width: '100%',
          height: '100%',
          tintColor: this.props.tintColor,
        }}
        source={require('../../../../assets/images/splash.gif')}
      />
    );
  }
}
